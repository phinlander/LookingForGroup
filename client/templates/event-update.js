// Event Update Template Methods 
Template.eventUpdate.helpers({
  isOwner: function() {
    return this.owner === Meteor.userId();
  }
})

// Event Create Page Helpers
Template.eventUpdate.events({
  // 'event element' jquery-style
  'submit #update-event': function(event) {
    // This function is called when the new task form is submitted
    var id = this._id;
    var name = event.target.name.value;
    var description = event.target.description.value;
    var roles = event.target.roles.value.split(',').map(function(str) {
      return str.trim();
    });
    var minSize = event.target.minSize.value;
    var maxSize = event.target.maxSize.value;
    var members = [Meteor.userId()];

    Meteor.call('updateEvent',id,name,description,roles,minSize,maxSize,members,function(error,event) {
      // Add a warning
      Meteor.call('addSuccess',Meteor.userId(),'Event '+name+' has been updated!');
      // Go back to the event's page
      Router.go('event.show',{_id:event._id});
    });
    
    return false;
  }
});