// Event Item Template Helpers
Template.eventListItem.events({
  'click .delete': function() {
    Meteor.call('deleteTask',this._id);
  },
  'click .toggle-private': function() {
    Meteor.call('setPrivate', this._id, ! this.private);
  }
});

Template.eventListItem.helpers({
  isOwner: function() {
    return this.owner === Meteor.userId();
  },
  memberCount: function() {
    return this.members.length;
  }
});