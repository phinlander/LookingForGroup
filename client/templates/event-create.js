// Event Create Page Helpers
Template.eventCreate.events({
  // 'event element' jquery-style
  'submit #new-event': function(event) {
    // This function is called when the new task form is submitted
    var name = event.target.name.value;
    var description = event.target.description.value;
    var roles = event.target.roles.value.split(',');
    var minSize = event.target.minSize.value;
    var maxSize = event.target.maxSize.value;
    var members = [Meteor.userId()];

    Meteor.call('addEvent',name,description,roles,minSize,maxSize,members,function(error,event) {
      // Add a warning
      Meteor.call('addSuccess',Meteor.userId(),'Event '+event.name+' has been created!');
      // Go to the new event's page
      Router.go('event.show',{_id:event._id});
    });
    
    return false;
  }
});