// Event Show Template Methods
Template.eventShow.helpers({
  isOwner: function() {
    return this.owner === Meteor.userId();
  },
  memberCount: function() {
    return this.members.length;
  }
})