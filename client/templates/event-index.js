// Event Index Page Methods
Template.eventIndex.helpers({
  events: function() {
    // Return all the events
    return Events.find({}, {sort: {createdAt: -1}});
  }
});