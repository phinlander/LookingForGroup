// Client subscriptions
Meteor.subscribe('groups');

Template.group.events({
  
});

Template.group.helpers({
  isOwner: function() {
    return this.owner === Meteor.userId();
  },
  membercount: function() {
    return this.members.length;
  }
})

GroupController = ApplicationController.extend({
  show: function() {
    this.render('GroupShow');
  },
  index: function() {
    this.render('GroupIndex');
  }
});