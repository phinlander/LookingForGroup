// Client subscriptions
Meteor.subscribe('events');

// Router controls
EventController = ApplicationController.extend({
  show: function() {
    this.render('EventShow', {
      data: function() {
        return Events.findOne({_id: this.params._id});
      }
    });
  },
  index: function() {
    this.render('EventIndex', {});
  },
  create: function() {
    this.render('EventCreate', {});
  },
  update: function() {
    this.render('EventUpdate', {
      data: function() {
        return Events.findOne({_id: this.params._id});
      }
    });
  },
  remove: function() {
    // Grab the event from the DB
    var event = Events.findOne({_id: this.params._id});
    // See if event exists
    if(!event) {
      Router.go('index');
      throw new Meteor.Error(
        'event-does-not-exists',
        'Event '+this.params._id+' does not exist'
      );
    }
    // Make sure this is the owner
    if(event.owner !== Meteor.userId()) {
      console.log(event.owner);
      console.log(Meteor.userId());
      Router.go('index');
      throw new Meteor.Error('not-authorized');
    }
    // Remove the entry
    Meteor.call('removeEvent',event._id, function(error,event) {
      Meteor.call('addSuccess',Meteor.userId(),'Event '+event.name+' has been deleted!');
    });
    Router.go('event.index');
  }
});