Meteor.publish('events',function() {
  return Events.find({
    $or: [
      { private: {$ne: true} },
      { owner: this.userId }
    ]
  });
});

Meteor.publish('groups', function() {
  return Groups.find({});
});

Meteor.publish('warnings',function() {
  return Warnings.find({
    owner: this.userId
  });
});