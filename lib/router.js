Router.route('index', {
  path: '/',
  controller: 'IndexController'
});

Router.route('event.index', {
  path: '/events',
  controller: 'EventController',
  action: 'index'
});

Router.route('event.create' ,{
  path: '/events/create',
  controller: 'EventController',
  action: 'create'
});

Router.route('event.show', {
  path: 'events/:_id',
  controller: 'EventController',
  action: 'show'
});

Router.route('event.update', {
  path: '/events/:_id/update',
  controller: 'EventController',
  action: 'update'
});

Router.route('event.remove', {
  path: '/events/:_id/remove',
  controller: 'EventController',
  action: 'remove'
});

Router.route('group.index', {
  path: '/events/:_id/groups',
  controller: 'GroupController',
  action: 'index'
});

Router.route('group.create', {
  path: '/events/:_id/groups/create',
  controller: 'GroupController',
  action: 'create'
});

Router.route('group.show', {
  path: '/events/:_id/groups/:groupId',
  controller: 'GroupController',
  action: 'show'
});

Router.route('group.update', {
  path: '/events/:_id/groups/:groupId/update',
  controller: 'GroupController',
  action: 'update'
});

Router.route('group.remove', {
  path: '/events/:_id/groups/:groupId/remove',
  controller: 'GroupController',
  action: 'remove'
});

var checkLoggedIn = function() {
  if(!Meteor.user() && !Meteor.loggingIn()) {
    Router.go('index');
  }
  this.next();
}

var checkWarnings = function() {
  if(Meteor.user()) {
    Warnings.findOne();
  }
  this.next();
}

Router.onBeforeAction(checkLoggedIn,{except:['index','event.index']});
Router.onBeforeAction(checkWarnings);