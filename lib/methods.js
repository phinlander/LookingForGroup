Meteor.methods({
  addEvent: function(name,description,roles,minSize,maxSize) {
    // Make sure the user is logged in before inserting a task
    if(! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    var id = Events.insert({
      name: name,
      description: description,
      createdAt: new Date(),
      owner: Meteor.userId(),
      username: Meteor.user().username,
      roles: roles,
      minSize: minSize || 0,
      maxSize: maxSize || Infinity,
      members: [Meteor.userId()]
    });
    
    return Events.findOne({_id: id})
  },
  updateEvent: function(id,name,description,roles,minSize,maxSize) {
    // Make sure the user is the owner before anything goes through
    if(Events.findOne({_id: id}).owner !== Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    Events.update({_id: id},{
      $set: {
        name: name,
        description: description,
        lastUpdatedAt: new Date(),
        roles: roles,
        minSize: minSize,
        maxSize: maxSize
      }
    });
    
    return Events.findOne({_id: id});
  },
  removeEvent: function(id) {
    // Get the event so you can check the owner
    var event = Events.findOne({_id: id});
    if(event.owner !== Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    // Remove the event from MongoDB
    Events.remove({_id:id});
    
    return event;
  },
  setEventPrivate: function(eventId, setToPrivate) {
    // Make sure only the task owner can make a task private
    if(Events.findOne(eventId).owner !== Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    // Update the privacy of the event
    Events.update(eventId, { $set: { private: setToPrivate } });
  },
  
  addGroup: function(name,description,owner) {
    // Make sure the user is logged in
    if(! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    Groups.insert({
      name: name,
      description: description,
      createdAt: new Date(),
      owner: Meteor.userId(),
      username: Meteor.user().username,
      members: [Meteor.userId()],
      open: true
    })
  },
  
  deleteGroup: function(groupId) {
    var group = Groups.findOne(groupId);
    if(group.owner !== Meteor.userId()) {
      // Cannot delete the group if not the owner
      throw new Meteor.Error('not-authorized');
    }
    Groups.remove(groupId);
  },
  
  addSuccess: function(userId,warningMessage) {
    if(! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    
    Warnings.insert({
      owner: userId,
      warningType: 'success',
      warningHeader: 'Success!',
      warningMessage: warningMessage,
      createdAt: new Date()
    })
  },
  
  removeSuccess: function(id) {
    if(Warnings.findOne({_id: id}).owner !== Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    
    Warnings.remove({_id:id});
  },
  
  addWarning: function(userId,warningMessage) {
    if(! Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    
    Warnings.insert({
      owner: userId,
      warningType: 'warning',
      warningHeader: 'Warning!',
      warningMessage: warningMessage,
      createdAt: new Date()
    })
  },
  
  removeWarning: function(id) {
    if(Warnings.findOne({_id: id}).owner !== Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    
    Warnings.remove({_id:id});
  },
})